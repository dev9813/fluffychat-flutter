abstract class SettingKeys {
  static const String jitsiInstance = 'chat.fluffy.jitsi_instance';
  static const String wallpaper = 'chat.fluffy.wallpaper';
  static const String renderHtml = 'chat.fluffy.renderHtml';
  static const String hideRedactedEvents = 'chat.fluffy.hideRedactedEvents';
  static const String hideUnknownEvents = 'chat.fluffy.hideUnknownEvents';
  static const String sentry = 'sentry';
  static const String theme = 'theme';
  static const String amoledEnabled = 'amoled_enabled';
  static const String codeLanguage = 'code_language';
  static const String showNoGoogle = 'chat.fluffy.show_no_google';
  static const String databasePassword = 'database-password';
}
